#About
// It is a small Java application that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then prints them into standard output.


#Build-Usage
//to build the app you should run this command in the right folder
 mvn clean compile assembly:single

#Run-Usage
//then run runnuable java jar file in target folder by using command below make sure you are in right folder and wrote right jar file and don't forget to pass 1 to 10 arguments
java -jar sortingAppProject-1.0-jar-with-dependencies.jar