package com.epam.training;


import com.epam.training.sort.Sorting4CornerCaseTest;
import com.epam.training.sort.SortingBadInputTest;
import com.epam.training.sort.SortingParametrizedTest;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;

public class AppTest {

    @Test
    public void main() {
        JUnitCore junit = new JUnitCore();

        Result result1 = junit.run(SortingBadInputTest.class);

        assertEquals(0, result1.getFailureCount());
        assertEquals(4, result1.getRunCount());


        Result result2 = junit.run(SortingParametrizedTest.class);

        assertEquals(0, result2.getFailureCount());
        assertEquals(4, result2.getRunCount());

        Result result3 = junit.run(Sorting4CornerCaseTest.class);

        assertEquals(0, result3.getFailureCount());
        assertEquals(4, result3.getRunCount());
    }
}