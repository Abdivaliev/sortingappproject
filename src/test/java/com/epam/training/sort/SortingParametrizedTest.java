package com.epam.training.sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingParametrizedTest {
    Sort sort = new Sort();

    private String[] input;
    private String[] expectedOutput;

    public SortingParametrizedTest(String[] input, String[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"7","6","7"}, new String[]{"6","7","7"}},
                {new String[]{"5", "5", "5", "5", "5", "5"}, new String[]{"5", "5", "5", "5", "5", "5"}},
                {new String[]{"14", "13", "12", "11", "10", "9"}, new String[]{ "9", "10", "11", "12", "13", "14"}},
                {new String[]{"8", "7", "6", "5", "4", "3", "2", "1"}, new String[]{"1", "2", "3", "4", "5", "6", "7", "8"}}
        });
    }

    @Test
    public void testSortStringNumbers() {
        sort.sort(input);
        assertArrayEquals(expectedOutput, input);
    }
}