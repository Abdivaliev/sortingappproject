package com.epam.training.sort;


import org.junit.Test;

public class SortingBadInputTest {

    Sort sort = new Sort();

    @Test(expected = IllegalArgumentException.class)
    public void testNullInput() {sort.sort(null);}

    @Test(expected = IllegalArgumentException.class)
    public void testFractionalInput() {
        sort.sort(new String[]{"1.5"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonDigitalInput() {
        sort.sort(new String[]{"a"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testALongInput() {
        sort.sort(new String[]{"6402373705728000"});
    }


}
