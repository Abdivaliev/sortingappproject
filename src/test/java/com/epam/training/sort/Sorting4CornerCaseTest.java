package com.epam.training.sort;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class Sorting4CornerCaseTest {
    Sort sort = new Sort();

    //One out of four corner cases is covered in unit tests.
    @Test(expected = IllegalArgumentException.class)
    public void testEmptyInput() {
        sort.sort(new String[]{});
    }

    //Two out of four corner cases are covered in unit tests.
    @Test
    public void testSingleInput() {
        String[] args = {"1"};
        sort.sort(args);
        assertArrayEquals(args,args);
    }

    //Three out of four corner cases are covered in unit tests.
    @Test(expected = IllegalArgumentException.class)
    public void testTenInput() {
        sort.sort(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
    }

    //Four out of four corner cases are covered in unit tests.
    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenInput() {
        sort.sort(new String[]{"15","14", "13", "12", "11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"});
    }
}
