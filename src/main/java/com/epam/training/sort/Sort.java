package com.epam.training.sort;

import java.util.Arrays;

public class Sort {
    public void sort(String[] args) {

        try {
            if (args.length > 9 || args.length == 0) {
                throw new IllegalArgumentException();
            }
            int[] array = new int[args.length];

            for (int i = 0; i < args.length; i++) {
                array[i] = Integer.parseInt(args[i]);
            }
            for (int i = 0; i < array.length; i++) {

                for (int j = i + 1; j < array.length; j++) {
                    if (array[i] > array[j]) {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
            for (int i = 0; i < array.length; i++) {
                args[i] = String.valueOf(array[i]);
            }
            System.out.println(Arrays.toString(args));
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

}
